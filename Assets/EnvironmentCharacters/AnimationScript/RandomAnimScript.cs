﻿using UnityEngine;
using System.Collections;

public class RandomAnimScript : MonoBehaviour {

	private int maxAnimState;
	private int stateNum;

	private const string STATE_KEY = "StateNum";

	private float stateDuration;
	private float timer;

	private Animator myController;

	// Use this for initialization
	void Start () {
		myController = this.GetComponent<Animator> ();
		SetMaxAnimState ();
		SetRandomState ();
	}
	
	// Update is called once per frame
	void Update () {
		timer += 1 * Time.deltaTime;

		if (timer >= stateDuration) {
			SetRandomState ();
		}
	}

	private void SetMaxAnimState(){
		if (myController.GetLayerName (0) == "Sitting")
			maxAnimState = 13;
		else if (myController.GetLayerName (0) == "Standing")
			maxAnimState = 16;
		else if (myController.GetLayerName (0) == "SittingNormal" || myController.GetLayerName (0) == "StandingNormal")
			maxAnimState = 11;
	}

	private void SetRandomState(){
		stateNum = Random.Range (0, maxAnimState);
		myController.SetInteger (STATE_KEY, stateNum);

		stateDuration = myController.GetCurrentAnimatorStateInfo (0).length;

		timer = 0.0f;
	}
}
