﻿using UnityEngine;
using System.Collections;

public class AnimationScript : MonoBehaviour {

	private Animator myController;
	private int currentState = 0;
	private int subState = 0;

	private float timer = 0.0f;
	private float stateDuration;

	private const string cheering = "Cheering";
	private const string victory = "Victory";

	private const string MAIN_STATE = "MainState";
	private const string SUB_STATE = "Substate";

	void Awake(){
		myController = this.GetComponent<Animator> ();
		SetRandomState ();

	}

	void Update(){
		timer += 1.0f * Time.deltaTime;
		if (timer > stateDuration) {
			SetRandomState ();
		}
	}

	private void SetRandomState(){
		currentState = Random.Range (0, 5);
		if(currentState > 2)
			subState = Random.Range (0, 6);

		myController.SetInteger (MAIN_STATE, currentState);
		myController.SetInteger (SUB_STATE, subState);

		stateDuration = myController.GetCurrentAnimatorStateInfo (0).length;

		timer = 0.0f;
	}


}
