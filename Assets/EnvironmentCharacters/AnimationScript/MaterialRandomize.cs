﻿using UnityEngine;
using System.Collections;

public class MaterialRandomize : MonoBehaviour {

	[SerializeField]
	private Material[] CharacterVariations;

	private SkinnedMeshRenderer myRenderer;

	void Awake(){
		myRenderer = this.GetComponent<SkinnedMeshRenderer> ();
		myRenderer.material = CharacterVariations [Random.Range (0, CharacterVariations.Length)];
	}
}
