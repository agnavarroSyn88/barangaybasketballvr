Shader "TSF/Ramp" 
{
    Properties 
    {
		[MaterialToggle(_TEX_ON)] _DetailTex ("Enable Detail texture", Float) = 0 	//1
		_MainTex ("Detail", 2D) = "white" {}        								//2
		_Ramp ("Ramp", 2D) = "gray" {}        								//2
		_ToonShade ("Shade", 2D) = "white" {}  										//3
		[MaterialToggle(_COLOR_ON)] _TintColor ("Enable Color Tint", Float) = 0 	//4
		_Color ("Base Color", Color) = (1,1,1,1)									//5	
		[MaterialToggle(_VCOLOR_ON)] _VertexColor ("Enable Vertex Color", Float) = 0//6        
		_Brightness ("Brightness 1 = neutral", Float) = 1.0							//7	
    }
   
    Subshader 
    {
    	//Tags { "RenderType"="Opaque" }
    	Tags { "RenderType"="Opaque" }
		LOD 250
    	ZWrite On
	   	Cull Back
		Lighting On
		Fog { Mode Off }
		
        Pass 
        {
            Name "BASE"
            CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag
                #pragma fragmentoption ARB_precision_hint_fastest
                #include "UnityCG.cginc"
                #pragma glsl_no_auto_normalization
                #pragma multi_compile _TEX_OFF _TEX_ON
                #pragma multi_compile _COLOR_OFF _COLOR_ON

                
                #if _TEX_ON
                sampler2D _MainTex;
				half4 _MainTex_ST;

				sampler2D _Ramp;
				half4 _Ramp_ST;
				#endif
				
                struct appdata_base0 
				{
					float4 vertex : POSITION;
					float3 normal : NORMAL;
					float4 texcoord : TEXCOORD0;
				};
				
                 struct v2f 
                 {
                    float4 pos : SV_POSITION;
                    #if _TEX_ON
                    half2 uv : TEXCOORD0;
                    #endif
                    half2 uvn : TEXCOORD1;
                 };
               
                v2f vert (appdata_base0 v)
                {
                    v2f o;
                    o.pos = mul ( UNITY_MATRIX_MVP, v.vertex );
                    float3 n = mul((float3x3)UNITY_MATRIX_IT_MV, normalize(v.normal));

					normalize(n);

                    n = n * float3(0.5,0.5,0.5) + float3(0.5,0.5,0.5);
                    //float3 gray = float3(0,0,0);
                    //n = n * gray + gray;

                    o.uvn = n.xy;
                     #if _TEX_ON
                    o.uv = TRANSFORM_TEX ( v.texcoord, _MainTex );
                    #endif
                    return o;
                }
				
//              #pragma surface surf ToonRamp
// 				sampler2D _Ramp;
//
// 				// custom lighting function that uses a texture ramp based
//				// on angle between light direction and normal
//				#pragma lighting ToonRamp exclude_path:prepass
//				inline half4 LightingToonRamp (SurfaceOutput s, half3 lightDir, half atten)
//				{
//				    #ifndef USING_DIRECTIONAL_LIGHT
//				    lightDir = normalize(lightDir);
//				    #endif
//				 
//				    half d = dot (s.Normal, lightDir)*0.5 + 0.5;
//				    half3 ramp = tex2D (_Ramp, float2(d,d)).rgb;
//				 
//				    half4 c;
//				    c.rgb = s.Albedo * _LightColor0.rgb * ramp * (atten * 2);
//				    c.a = 0;
//				    return c;
//				}

//				sampler2D _MainTex;
//				float4 _Color;
//				 
//				struct Input {
//				    float2 uv_MainTex : TEXCOORD0;
//				};
//				 
//				void surf (Input IN, inout SurfaceOutput o) {
//				    half4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
//				    o.Albedo = c.rgb;
//				    o.Alpha = c.a;
//				}
//				// End Ramp

              	sampler2D _ToonShade;
                fixed _Brightness;
                
                #if _COLOR_ON
                fixed4 _Color;
                #endif
                
                fixed4 frag (v2f i) : COLOR
                {
					#if _COLOR_ON
					fixed4 toonShade = tex2D( _ToonShade, i.uvn ) * _Color;
					#else
					fixed4 toonShade = tex2D( _ToonShade, i.uvn );
					#endif
					
					#if _TEX_ON
					fixed4 detail = tex2D ( _MainTex, i.uv );
					fixed4 shadows = tex2D ( _Ramp, i.uv );
					return  toonShade * detail * _Brightness;
					//return  toonShade * detail * shadows * _Brightness * 1;
					#else
					//return  toonShade * _Brightness;
					return  toonShade * _Brightness * 1;
					#endif
                }
            ENDCG
        }
    }
    Fallback "Legacy Shaders/Diffuse"
}