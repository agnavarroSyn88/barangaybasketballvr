﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    public GameObject bullet;
    public AudioClip explodeSFX;
    public LayerMask ballMask;
    AudioSource audioSource;
    // Use this for initialization
	void Start () {
        audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
        if (GameManager.instance.currentGameState == GameManager.GameState.Game)
        {
            if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began || Input.GetMouseButtonDown(0) || Input.GetButtonDown("Submit"))
            {
                audioSource.PlayOneShot(explodeSFX);
                RaycastHit hit;
                if (Physics.Raycast(transform.position, transform.forward, out hit, ballMask))
                {
                    if (hit.transform.gameObject.GetComponent<Ball>() != null)
                    {
                        hit.transform.gameObject.GetComponent<Ball>().Explode(hit.point);
                    }
                }
            }
        }

       
	
	}

    public void ShootBullet()
    {
        Instantiate(bullet, transform.position, transform.rotation);
    }
}
