﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class SplashScreen : MonoBehaviour {

    public Animator FadeAnimator;
    public string sceneToLoad;

    // Use this for initialization
    void Start () {
        FadeAnimator.SetTrigger("FadeIn");
        Invoke("FadeOut", 3f);
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void FadeOut()
    {
        FadeAnimator.SetTrigger("FadeOut");
        Invoke("LoadGame", 2f);
        
    }

    void LoadGame()
    {
        SceneManager.LoadScene(sceneToLoad);
    }
}
