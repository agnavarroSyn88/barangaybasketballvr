﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class QuitPanel : MonoBehaviour {

    public GameObject ExitPanel;
    public AudioClip tapButton;
    public GameObject EventSys;

    bool panelOn = false;
    bool keyPressed = false;
    AudioSource audioSource;
    StandaloneInputModule sim;
	// Use this for initialization
	void Start () {
        audioSource = GetComponent<AudioSource>();
        sim = EventSys.GetComponent<StandaloneInputModule>();

    }
	
	// Update is called once per frame
	void Update () {

        if (GvrViewer.Instance.BackButtonPressed || (Input.GetKeyUp(KeyCode.Escape) && !keyPressed) )
        {
            keyPressed = true;
            sim.enabled = true;
            //Time.timeScale = 0f;
            if (!panelOn)
            {
                audioSource.PlayOneShot(tapButton);
                panelOn = true;
                ExitPanel.SetActive(true);
            }
            else
            {
                Cancel();
            }
        }
        else if(Input.GetKeyDown(KeyCode.Escape))
        {
            keyPressed = false;
        }



    }

    public void Quit()
    {
      
        if (Application.platform == RuntimePlatform.Android)
        {
            audioSource.PlayOneShot(tapButton);
            System.Diagnostics.Process.GetCurrentProcess().Kill();
        }
    
    }

    public void Cancel()
    {
        //Time.timeScale = 0f;
        sim.enabled = false;
        audioSource.PlayOneShot(tapButton);
        panelOn = false;
        ExitPanel.SetActive(false);
    }


}
