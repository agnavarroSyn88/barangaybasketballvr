﻿using UnityEngine;
using System.Collections;

public class ShooterBehavior : MonoBehaviour {

    public Animator ShooterAnimator;
    public Ball ball;
    public Transform currentTarget;
    public Ball[] balls;
    public Transform[] Targets;
    public FollowTarget targetMarker;

    public float ShootEveryNSeconds = 3f;
    public float startDelay = 5f;

    public bool canShoot = false;
    bool success;
    float time = 0f;
    int iCurrentBall = 0;
	// Use this for initialization
	void Awake () {
        ShooterAnimator.SetTrigger("State_Offensive");
        //Invoke("Begin",5f);
        ball = balls[iCurrentBall];
        targetMarker.Target = ball.gameObject;
	}

    public void Begin()
    {
        SetCurrentTarget();
        Invoke("setCanShoot", startDelay);
        

    }

    void setCanShoot()
    {
        canShoot = true;
        GameManager.instance.currentGameState = GameManager.GameState.Game;

    }
	
	// Update is called once per frame
	void FixedUpdate () {
       

        
        if (GameManager.instance.currentGameState == GameManager.GameState.Game)
        {
            if (canShoot && ball.currentState == Ball.BallState.Idle)
            {
                StartCoroutine(ShootInterval());
                SetCurrentTarget();
                ball.targetPos = currentTarget.position;
                ball.time = Random.Range(1.0f, 1.5f);
                Shoot();
                
                
            }

            //time += Time.fixedDeltaTime;
            //Debug.Log("Time: " + time);

            //if (Mathf.(time, ShootEveryNSeconds + ball.time + 0.7f))
            //{
            //    canShoot = true;
            //    time = 0f;
            //}
            //else
            //{
            //    canShoot = false;
            //}

        }
       



    }

    public void Shoot()
    {
        Debug.Log("Shoot");
        ShooterAnimator.SetTrigger("Shoot");
        Invoke("startShoot", 0.7f);
    }

    void SetCurrentTarget()
    {
        int i = Random.Range(0, Targets.Length);
        if (Targets[i].gameObject.name.StartsWith("Success"))
        {
            success = true;
        }
        else if (Targets[i].gameObject.name.StartsWith("Miss"))
        {
            success = false;
        }
        currentTarget = Targets[i];
    }

    void startShoot()
    {
        
       
        ball.Shoot(success, currentTarget.position);
        

    }

    IEnumerator ShootInterval()
    {
        canShoot = false;
        yield return new WaitForSeconds(ShootEveryNSeconds + ball.time + 0.7f);
        canShoot = true;
    }

    public void getNextBall()
    {
        iCurrentBall++;
        if(iCurrentBall < balls.Length)
        {
            ball = balls[iCurrentBall];
        }
        else
        {
            iCurrentBall = 0;
            ball = balls[iCurrentBall];
        }
        ball.gameObject.SetActive(true);
        ball.activeBallMesh.SetActive(true);
        targetMarker.Target = ball.gameObject;
    }
}
