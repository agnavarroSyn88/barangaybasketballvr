﻿using UnityEngine;
using System.Collections;

public class DontShootTrigger : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Ball"))
        {

            Ball ball = other.GetComponent<Ball>();
            ball.inTheRing = true;
           
        }
    }
}
