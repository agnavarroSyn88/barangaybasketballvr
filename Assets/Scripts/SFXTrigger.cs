﻿using UnityEngine;
using System.Collections;

public class SFXTrigger : MonoBehaviour {

    public AudioClip clip;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Ball"))
        {
          other.GetComponent<Ball>().audioSource.PlayOneShot(clip);
        }
    }

}
