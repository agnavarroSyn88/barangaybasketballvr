﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

using UnityEngine.VR;
using System.Collections;

public class GameManager : MonoBehaviour {

    public static GameManager instance;
    public enum GameState
    {
        PreGame,
        Game,
        Results
    }

    public int Lives = 3;
    public int Score = 0;
    public int HiScore = 0;

    public Text LivesText;
    public Text ScoreText;
    public Text ResultScoreText;
    public Text HiScoreText;
    public GameObject StartButton;
    public GameObject ResultCanvas;

    public Animator FadeAnimator;
    public Animator buzzerAnimator;
    public Animator whistleAnimator;
    public GameState currentGameState;
    public ShooterBehavior[] shooters;
    public PhysicsRaycaster playerPhysicsMasks;

    public AudioClip buzzer;
    public AudioClip whistle;
    public AudioClip win;
    public AudioClip lose;

    public const string HISCORE_KEY = "KEY_HISCORE";

    bool shownResult = false;
    bool gameStarted = false;
    AudioSource audioSource;
    int prevHiScore = 0;
    
	// Use this for initialization

    void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);
        CheckPlayerPrefHiScore();
        

        
    }

	void Start () {
        currentGameState = GameState.PreGame;
        FadeAnimator.SetTrigger("FadeIn");
        audioSource = GetComponent<AudioSource>();
        //Cursor.visible = false;
	
	}
	
	// Update is called once per frame
	void Update () {
        LivesText.text = Lives.ToString();
        ScoreText.text = Score.ToString();
        HiScoreText.text = HiScore.ToString();

      
       

        if (Lives <=0 && currentGameState == GameState.Game)
        {
            Lives = 0;
            currentGameState = GameState.Results;
        }

       

    }

    void FixedUpdate()
    {
        switch (currentGameState)
        {
            case GameState.PreGame:
                break;
            case GameState.Game:
                if (Score >= HiScore)
                {
                    HiScore = Score;
                }
                break;
            case GameState.Results:
                ShowResults();
                break;
            default:
                break;
        }
    }

    public void StartGame()
    {
        audioSource.PlayOneShot(buzzer);
    }

    void ShowResults()
    {
        //ResultScoreText.text = "SCORE\n" + Score.ToString();

        if (!shownResult)
        {
            //ResultCanvas.SetActive(true);
            shownResult = true;
            for (int i = 0; i < shooters.Length; i++)
            {
                if (shooters[i].gameObject.activeInHierarchy)
                {
                    
                    if (Score <= prevHiScore)
                    {
                        audioSource.PlayOneShot(lose);
                        shooters[i].ShooterAnimator.SetTrigger("Win");
                        shooters[i].ball.gameObject.SetActive(false);
                    }
                    else if (Score > prevHiScore)
                    {
                        audioSource.PlayOneShot(win);
                        shooters[i].ShooterAnimator.SetTrigger("Lose");
                        shooters[i].ball.gameObject.SetActive(false);
                    }
                }
            }
            //TryAgain();
        }
       
    }

    public void TryAgain()
    {
        if (currentGameState == GameState.Results)
        {
            audioSource.PlayOneShot(whistle);
            whistleAnimator.SetTrigger("ClickTrigger");
            //StartButton.SetActive(true);
            currentGameState = GameState.PreGame;
            for (int i = 0; i < shooters.Length; i++)
            {
                if (shooters[i].gameObject.activeInHierarchy)
                {
                    shooters[i].ball.gameObject.SetActive(true);
                    shooters[i].ball.ResetBall();
                    shooters[i].canShoot = false;
                    shooters[i].ShooterAnimator.SetTrigger("State_Offensive");
                    shooters[i].ball.activeBallMesh.SetActive(true);
                }

            }
            
            PlayerPrefs.SetInt(HISCORE_KEY, HiScore);
            prevHiScore = HiScore;
            Lives = 5;
            Score = 0;
            gameStarted = false;
            // ResultCanvas.SetActive(false);
        }
        //else if (currentGameState == GameState.Game)
        //{
        //    currentGameState = GameState.Results;
        //    for (int i = 0; i < shooters.Length; i++)
        //    {
        //        if (shooters[i].gameObject.activeInHierarchy)
        //        {
        //            shooters[i].ball.line.enabled = false;
        //        }
        //    }
        //}
    }

    public void Highlight(GameObject target)
    {
        Material targetMat = target.GetComponent<Renderer>().material;
        if (targetMat.HasProperty("_OutlineColor"))
        {
            Debug.Log("Has Outline Color");
            targetMat.SetColor("_OutlineColor", Color.white);
        }
        if (targetMat.HasProperty("_Outline"))
        {
            Debug.Log("Has Outline");
            targetMat.SetFloat("_Outline", 1.0f);
        }
    }

    public void UnHighlight(GameObject target)
    {
        Material targetMat = target.GetComponent<Renderer>().material;
        targetMat.SetColor("_OutlineColor", Color.black);
        targetMat.SetFloat("_Outline", 0.2f);
    }

    public void Reset()
    {
        if (currentGameState == GameState.PreGame && !gameStarted)
        {
            gameStarted = true;           
            shownResult = false;
            audioSource.PlayOneShot(buzzer);
            buzzerAnimator.SetTrigger("ClickTrigger");
            for (int i = 0; i < shooters.Length; i++)
            {
                if (shooters[i].gameObject.activeInHierarchy)
                {
                    shooters[i].Begin();
                }
            }
        }
    }

    void CheckPlayerPrefHiScore()
    {

        if(PlayerPrefs.HasKey(HISCORE_KEY))
        {
            prevHiScore = PlayerPrefs.GetInt(HISCORE_KEY);
            HiScore = prevHiScore;
            
        }
    }

    void OnApplicationPause(bool pauseState)
    {
        if(pauseState)
        {
           
            PlayerPrefs.SetInt(HISCORE_KEY, HiScore);
        }
    }
}
