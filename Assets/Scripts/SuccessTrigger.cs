﻿using UnityEngine;
using System.Collections;

public class SuccessTrigger : MonoBehaviour {

    public Animator RingAnim;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Ball"))
        {
            
            Ball ball = other.GetComponent<Ball>();
            ball.currentState = Ball.BallState.Idle;
            ball.targetMarker.SetActive(false);
            ball.targetMarker.transform.position = ball.targetOriginalPos;
            ball.Detector.enabled = false;
            //ball.explosion.Play();
            RingAnim.SetTrigger("Shot");
            ball.trail.enabled = false;
            ball.UndrawTrajectory();
        }
    }
}
