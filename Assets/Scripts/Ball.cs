﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour
{

    public enum BallState
    {
        Idle,
        Shot,
        Finished
    }

    public float time = 1.5f;
    public Transform startPosition;
    public BallState currentState;
    public GameObject activeBallMesh;
    private Transform originalParent;
    public ShooterBehavior shooter;
    public LineRenderer line;
    public GameObject targetMarker;
    public Material successMaterial;
    public Material missMaterial;
    public AudioClip explodeSFX;
    public AudioClip miss;
    public Animator missText;
    public LayerMask boardMask;
    public LayerMask floorMask;
    public LayerMask ringMask;
    public LayerMask ballMask;
    public LayerMask withOutBallMask;
    public GazeInputModule gazeInput;

    [HideInInspector]
    public Rigidbody rb;
    [HideInInspector]
    public TrailRenderer trail;
    [HideInInspector]
    public bool succeed;
    [HideInInspector]
    public Collider sphereCol;
    [HideInInspector]
    public AudioSource audioSource;

    public Collider Detector;

    Vector3 initialVelocity;
    int lineSegments = 18;

    [SerializeField]
    public ParticleSystem explosion;

    [HideInInspector]
    public Vector3 targetPos;
    
    public bool inTheRing = false;

    Material ballMaterial;

    [HideInInspector]
    public Vector3 targetOriginalPos;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        trail = GetComponent<TrailRenderer>();
        sphereCol = GetComponent<Collider>();
        audioSource = GetComponent<AudioSource>();
        ballMaterial = activeBallMesh.GetComponent<Renderer>().material;
        ballMaterial.shader = Shader.Find("TSF/BaseOutline1");
        targetOriginalPos = targetMarker.transform.position;
    }

    // Use this for initialization
    void Start()
    {
        originalParent = transform.parent;
        currentState = BallState.Idle;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void FixedUpdate()
    {
        switch (currentState)
        {
            case BallState.Idle: activeBallMesh.SetActive(true); GameManager.instance.playerPhysicsMasks.eventMask = withOutBallMask; break;
            case BallState.Shot: GameManager.instance.playerPhysicsMasks.eventMask = ballMask; break;
            case BallState.Finished: GameManager.instance.playerPhysicsMasks.eventMask = withOutBallMask; break;
        }
    }

    public void Stop()
    {
        rb.isKinematic = true;
        rb.Sleep();
        rb.velocity = Vector3.zero;
        line.enabled = false;
        sphereCol.enabled = false;
        Detector.enabled = false;
        targetMarker.SetActive(false);
        targetMarker.transform.position = targetOriginalPos;
    }

    public void Message(string val)
    {
        Debug.Log(val);
    }

    public void Explode(Vector3 hitPos)
    {
        if (currentState == BallState.Shot)
        {
            
            Stop();
            explosion.Play();
            //transform.position = new Vector3(transform.position.x, gazeInput.pointerData.pointerCurrentRaycast.worldPosition.y, gazeInput.pointerData.pointerCurrentRaycast.worldPosition.z);
            transform.position = hitPos;
            trail.enabled = false;
            
            //audioSource.PlayOneShot(explodeSFX);
           
            activeBallMesh.SetActive(false);

            currentState = BallState.Finished;
            if (!succeed)
                GameManager.instance.Score++;
            else
            {
                //missText.SetTrigger("miss");
                audioSource.PlayOneShot(miss);
                GameManager.instance.Lives--;
            }
            StartCoroutine(WaitforReset());
        }

    }

    public void Shoot(bool success, Vector3 target)
    {
        transform.position = startPosition.position;
        if (success)
        {
            lineSegments = (int)(time / 0.1f) + 6;
            line.material = successMaterial;
        }
        else
        {
            lineSegments = (int)(time / 0.1f) + 9;
            line.material = missMaterial;
        }
        succeed = success;
        targetMarker.SetActive(true);
        setTrajectoryPoints();
        Time.timeScale = 0.9f;
        currentState = BallState.Shot;
        transform.parent = null;
        Detector.enabled = true;
        rb.isKinematic = false;
        trail.enabled = true;
        DrawTrajectory();
       

        rb.velocity = this.CalculateVelocity(startPosition.position, targetPos);


    }

    private Vector3 CalculateVelocity(Vector3 position, Vector3 targetPosition)
    {
        Vector3 b = targetPosition;
        Vector3 a = transform.position;
        Vector3 g = Physics.gravity;//new Vector3(0.0f, -1.0f, 0.0f);
        Vector3 velocity = ((b - a) / this.time) - 0.5f * g * this.time;
        return velocity;
    }

    public void DrawTrajectory()
    {
        //dBall.Shoot(true, targetPos);
        line.enabled = true;
        //HighlightBall();
        //setTrajectoryPoints();
    }

    public void UndrawTrajectory()
    {
        //dBall.GetComponent<TrailRenderer>().enabled = false;
        line.enabled = false;
        // UnHighlightBall();
    }

    public void HighlightBall()
    {
        if (ballMaterial.HasProperty("_OutlineColor"))
        {
            Debug.Log("Has Outline Color");
            ballMaterial.SetColor("_OutlineColor", Color.green);
        }
        if (ballMaterial.HasProperty("_Outline"))
        {
            Debug.Log("Has Outline");
            ballMaterial.SetFloat("_Outline", 2.0f);
        }
    }

    public void UnHighlightBall()
    {
        ballMaterial.SetColor("_OutlineColor", Color.black);
        ballMaterial.SetFloat("_Outline", 0.2f);
    }

    void setTrajectoryPoints()
    {
        Vector3 lastPos = startPosition.position;
        Vector3 velocity = CalculateVelocity(startPosition.position, targetPos);
        line.SetPosition(0, startPosition.position);
        line.SetVertexCount(lineSegments);

        float t = 0.0f;
        for (int i = 1; i < lineSegments; i++)
        {

            lastPos += velocity * t + 0.5f * Physics.gravity * t * t;
            velocity += Physics.gravity * t;
            RaycastHit hit;
            if (succeed)
            {
                if (Physics.Linecast(lastPos, (lastPos + (velocity * t)), out hit, ringMask))
                {
                    //velocity = Vector3.Reflect(velocity * 0.5f, hit.normal);
                    lastPos = hit.point;
                    line.SetPosition(i, lastPos);
                    //t += 0.01f;
                    //i++;
                    //lastPos += velocity * t + 0.5f * Physics.gravity * t * t;
                    //velocity += Physics.gravity * t;
                    //line.SetPosition(i, lastPos);
                    line.SetVertexCount(i + 1);
                    break;
                }
                if (Physics.Linecast(lastPos, (lastPos + (velocity * t)), out hit, boardMask))
                {
                    velocity = Vector3.Reflect(velocity * 0.5f, hit.normal);
                    lastPos = hit.point;
                }

                line.SetPosition(i, lastPos);
                t += 0.01f;
            }
            else
            {
                if (Physics.Linecast(lastPos, (lastPos + (velocity * t)), out hit, floorMask))
                {
                    //velocity = Vector3.Reflect(velocity * 0.5f, hit.normal);
                    lastPos = hit.point;
                    line.SetPosition(i, lastPos);
                    //t += 0.01f;
                    //i++;
                    //lastPos += velocity * t + 0.5f * Physics.gravity * t * t;
                    //velocity += Physics.gravity * t;
                    //line.SetPosition(i, lastPos);
                    line.SetVertexCount(i + 1);
                    break;
                }
                if (Physics.Linecast(lastPos, (lastPos + (velocity * t)), out hit, boardMask))
                {
                    velocity = Vector3.Reflect(velocity * 0.5f, hit.normal);
                    lastPos = hit.point;

                }
                line.SetPosition(i, lastPos);
                t += 0.01f;
            }


        }
    }

    public IEnumerator WaitforReset()
    {
        yield return new WaitForSeconds(1f);
        ResetBall();
    }

    public void ResetBall()
    {
        if (GameManager.instance.currentGameState == GameManager.GameState.Game ||
            GameManager.instance.currentGameState == GameManager.GameState.PreGame)
        {
            Time.timeScale = 1f;
            currentState = BallState.Idle;
            rb.isKinematic = true;
            rb.WakeUp();
            sphereCol.enabled = true;
            Detector.enabled = true;
            activeBallMesh.SetActive(true);
            transform.parent = originalParent;
            transform.localPosition = Vector3.zero;
            transform.eulerAngles = Vector3.zero;
            gameObject.SetActive(true);
            shooter.ShooterAnimator.SetTrigger("State_Offensive");
        }

    }
}
