﻿using UnityEngine;
using System.Collections;

public class DummyBall : MonoBehaviour {

    public float time = 1.5f;
    public Ball ball;
    public Vector3 startPos;
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        time = ball.time;
	}

    public void Shoot(bool success, Vector3 target)
    {
        
        transform.parent = null;
        GetComponent<Rigidbody>().isKinematic = false;
        GetComponent<TrailRenderer>().enabled = true;
        //this.trail.enabled = true;

        //ring.Shooting();

        //this.rigidbody.velocity = this.BallisticVelocity(this.target, this.TargetAngle(this.target));
        if (success)
        {
            GetComponent<Rigidbody>().velocity = this.CalculateVelocity(transform.position, target);
        }
        else
        {
            GetComponent<Rigidbody>().velocity = this.CalculateVelocity(transform.position, target);
        }
    }

    private Vector3 CalculateVelocity(Vector3 position, Vector3 targetPosition)
    {
        Vector3 b = targetPosition;
        Vector3 a = transform.position;
        Vector3 g = Physics.gravity;//new Vector3(0.0f, -1.0f, 0.0f);
        Vector3 velocity = ((b - a) / this.time) - 0.5f * g * this.time;
        return velocity;
    }
}
