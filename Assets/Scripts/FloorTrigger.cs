﻿using UnityEngine;
using System.Collections;

public class FloorTrigger : MonoBehaviour {

    public Transform originalParent;
    public Animator ShooterAnim;
    public ShooterBehavior shooter;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Ball"))
        {
           
            Ball ball = other.GetComponent<Ball>();
            ball.currentState = Ball.BallState.Finished;
            ball.targetMarker.SetActive(false);
            ball.targetMarker.transform.position = ball.targetOriginalPos;
            Time.timeScale = 1f;
            if (GameManager.instance.Lives > 0 && !ball.succeed)
            {
                ball.missText.SetTrigger("miss");
                ball.audioSource.PlayOneShot(ball.miss);
                GameManager.instance.Lives--;
            }
            ball.rb.isKinematic = true;
            ball.trail.enabled = false;
            ball.UndrawTrajectory();
            other.transform.parent = originalParent;
            other.transform.localPosition = Vector3.zero;
            other.transform.eulerAngles = Vector3.zero;
            
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Ball"))
        {
            if (GameManager.instance.currentGameState == GameManager.GameState.Game)
            {
                ShooterAnim.SetTrigger("State_Offensive");
                Ball ball = other.GetComponent<Ball>();
                //ball.inTheRing = false;
                //shooter.getNextBall();
                ball.currentState = Ball.BallState.Idle;
                ball.gameObject.SetActive(true);
                ball.activeBallMesh.SetActive(true);
                //other.gameObject.SetActive(false);
                //other.gameObject.SetActive(true);
            }
        }
    }
}
